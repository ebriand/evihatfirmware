 
import time, _thread, machine
from machine import UART, Pin, Timer
import network

## #################################
## ########## PARAMETERS  ##########
## #################################



## #################################
## ########## INITIALIZATION  ######
## #################################

tim = Timer()

led = Pin("LED", Pin.OUT)
pin_enable = Pin(12, Pin.OUT)
pin_diag = Pin(13, Pin.IN)
pin_dir = Pin(14, Pin.OUT)
pin_step = Pin(15, Pin.OUT)

led.value(0)
pin_enable.value(1)
pin_dir.value(0)
pin_step.value(0)

uart1 = UART(1, baudrate=115200, tx=Pin(8), rx=Pin(9), bits=8, parity=None, stop=1)

pwm_step = machine.PWM(pin_step)
pwm_step.freq(800)
pwm_step.duty_u16(32512)

ramp_state = 1
ramp = [100, 250, 1000, 2000 ]
ramp_index = 0
ramp_index_max = len(ramp)
freq_fullspeed = 1.0/2.0
freq_stop = 1.0/5.0
freq_ramp = 10.0

def ramp_callback(t):
    global ramp_state
    global ramp_index
    
    if ramp_state == 1:
        pin_enable.value(0)
        pwm_step.duty_u16(32512)
        tim.init(mode=Timer.PERIODIC, freq=freq_ramp, callback=ramp_callback)
        ramp_state = 2
    if ramp_state == 2:
        if ramp_index >= ramp_index_max:
            ramp_state = 3
        else:
            pwm_step.freq(ramp[ramp_index])
            ramp_index += 1
    elif ramp_state == 3:
        ramp_index -= 1
        tim.init(mode=Timer.PERIODIC, freq=freq_fullspeed, callback=ramp_callback)
        ramp_state = 4
    elif ramp_state == 4:
        tim.init(mode=Timer.PERIODIC, freq=freq_ramp, callback=ramp_callback)
        ramp_state = 5
    elif ramp_state == 5:
        if ramp_index < 0:
            ramp_state = 6
            pwm_step.duty_u16(0)
            pin_enable.value(1)
        else:
            pwm_step.freq(ramp[ramp_index])
            ramp_index -= 1
    elif ramp_state == 6:
        ramp_index = 0
        ramp_state = 1
        tim.init(mode=Timer.PERIODIC, freq=freq_stop, callback=ramp_callback)



def ramp_callback(t):
    print(t)

tim.init(mode=Timer.PERIODIC, freq=5, callback=ramp_callback)

## #################################
## ########## MAIN LOOP  ###########
## #################################

while True:
    pass


GCONF = 0x00
GSTAT = 0x01
IFCNT = 0x02
SLAVECONF = 0x03
OTP_PROG = 0x04
OTP_READ = 0x05
IOIN = 0x06
FACTORY_CONF = 0x07
IHOLD_IRUN = 0x10
TPOWER_DOWN = 0x11
TSTEP = 0x12
TPWMTHRS = 0x13
TCOOLTHRS = 0x14
VACTUAL = 0x22
SGTHRS = 0x40
SG_RESULT = 0x41
COOLCONF = 0x42
MSCNT = 0x6A
MSCURACT = 0x6B
CHOPCONF = 0x6C
DRV_STATUS = 0x6F
PWMCONF = 0x70
PWM_SCALE = 0x71
PWM_AUTO = 0x72

stepper_id = 0
reg = 0

from time import sleep

def compute_crc8_atm(datagram, initial_value=0):
    crc = initial_value
    # Iterate bytes in data
    for byte in datagram:
        # Iterate bits in byte
        for _ in range(0, 8):
            if (crc >> 7) ^ (byte & 0x01):
                crc = ((crc << 1) ^ 0x07) & 0xFF
            else:
                crc = (crc << 1) & 0xFF
        # Shift to next bit
        byte = byte >> 1
    return crc

def read_reg(mtr_id,reg):
    global uart1
    x = [0x55, 0, 0, 0]
    x[1] = mtr_id
    x[2] = reg
    x[3] = compute_crc8_atm(x[:-1])
    y = uart1.write(bytes(x))
    if y != len(x):
        print("Err in read")
        return False
    sleep(.01)
    if uart1.any():
        y = uart1.read(4)#read what it self send and trash it (RX and TX one line)
        y = uart1.read()
    else:
        print("No Resp.")
        y = 0
        sleep(.000005)
    return(y)


a = read_reg(0, IOIN)
print(a[3],a[4],a[5],a[6])

for i in bytearray(a):
    print("REG: ", hex(i))

uart1 = UART(1, baudrate=9600, tx=Pin(8), rx=Pin(9), bits=8, parity=None, stop=1)


dgram = bytes((0b0101_0000,0b0000_0000,0b0000_011_0,0b00000000))
"{}".format(dgram)
uart1.write(dgram);print(uart1.read(32));print(uart1.read(32))
"{0:b}".format(readback)

def send_command(cmd):
    uart1.write(cmd + b'\n')
    response = uart1.readline()
    return response

def set_microstep_mode(mode):
    cmd = 'V{}'.format(mode).encode()
    send_command(cmd)


led.value(1)

while True:
    print(pin_diag())

pin_enable.value(0)
print(pin_diag())
time.sleep(1)
print(pin_diag())
pin_enable.value(1)


pwm_step = machine.PWM(pin_step)
pwm_step.freq(200)
pwm_step.duty_u16(32512)


    

pin_enable.value(0)
pin_dir.value(0)
for i in range(5,600):
    pwm_step.freq(i*10)
    time.sleep_ms(10)

for i in range(5,600):
    pwm_step.freq(6000-i*10)
    time.sleep_ms(10)

pin_dir.value(1)

for i in range(5,600):
    pwm_step.freq(i*10)
    time.sleep_ms(10)

for i in range(5,600):
    pwm_step.freq(6000-i*10)
    time.sleep_ms(10)


print(pin_diag())
pin_enable.value(1)


time.sleep(2)
print(pin_diag())
pwm_step.freq(50)
pin_enable.value(1)


pwm_step.freq(1500)

for i in range(500):
    pwm_step.freq(600-i)
    time.sleep_ms(1)

pin_enable.value(1)
print(pin_diag())



for i in range(100):
    pin_step.value(1)
    pin_step.value(0)

print(pin_diag())      
pin_enable.value(1)    
print(pin_diag())  

for i in range(100):
    pin_step.value(1)
    time.sleep(0.1)
    pin_step.value(0)
    time.sleep(0.1)
    print(pin_diag(),i)
    
print(pin_diag())
pin_enable.value(1)
time.sleep(1)
print(pin_diag())

# def task(n, delay):
#     led = machine.Pin("LED", machine.Pin.OUT)
#     for i in range(n):
#         led.high()
#         time.sleep(delay)
#         led.low()
#         time.sleep(delay)
#     print('done')

#_thread.start_new_thread(task, (10, 0.5))




uart1 = UART(1, baudrate=9600, tx=Pin(8), rx=Pin(9))


led.value(1)

led = Pin("LED", Pin.OUT)
tim = Timer()

def tick(timer):
    global led
    led.toggle()

#tim.init(freq=1, mode=Timer.PERIODIC, callback=tick)






# txData = b'hello world\n\r'
# uart1.write(txData)
# time.sleep(0.1)
# rxData = bytes()
# while uart0.any() > 0:
#     rxData += uart0.read(1)
# 
# print(rxData.decode('utf-8'))
